package assignment5;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;

public class ScoreGUI extends JFrame implements ActionListener {
	
	/**
	 * @author cjp2790
	 * 8/31/2014
	 * This is the driver file for this assignment.
	 */
	
	JTextField[] jtxtScore = new JTextField[4];
	JTextField[] jtxtWeight = new JTextField[4];
	JButton jbtnCalScore = new JButton("Calcuate Score");
	JPanel jpnlScore = new JPanel(new GridLayout(5, 2));
	JTextField jtxtDisplay = new JTextField();
	JLabel labelScore = new JLabel("Score");
	JLabel labelWeight = new JLabel("Weight");
	JLabel[] label = new JLabel[10];
	int[] score = new int[4];
	double[] weight = new double[4];

	/**
	 * Defines a new ScoreGUI
	 */
	public ScoreGUI() {
		for (int i = 0; i < jtxtScore.length; i++) {
			jtxtScore[i] = new JTextField(6);
			jtxtWeight[i] = new JTextField(6);
			jpnlScore.add(jtxtScore[i]);
			jpnlScore.add(jtxtWeight[i]);
		}
		
		/**
		 * Positions the text-entry boxes and the text-output boxes within the application
		 */
		jbtnCalScore.addActionListener(this);
		add(jtxtDisplay, BorderLayout.NORTH);
		add(jpnlScore);
		add(jbtnCalScore, BorderLayout.SOUTH);
		setVisible(true);
		pack();
		jpnlScore.add(labelScore, BorderLayout.CENTER);
		jpnlScore.add(labelWeight, BorderLayout.CENTER);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		for (int i = 0; i < score.length; i++) {
			score[i] = Integer.parseInt(jtxtScore[i].getText());
			weight[i] = Double.parseDouble(jtxtWeight[i].getText());
		}
		jtxtDisplay.setText(ScoreCalculator.calScore(score, weight) + "");
	}

	public static void main(String[] args) {
		new ScoreGUI();
	}
}
